<?php 

/** 
* Generated at: 2018-02-14T16:11:39+01:00
* Inheritance: no
* Variants: no
* Changed by: admin (2)
* IP: 127.0.0.1


Fields Summary: 
- Nom [input]
- Prenom [input]
- Mail [input]
- Commentaire [textarea]
- Livraison [select]
- Date [datetime]
- Nombre [numeric]
- Volume [select]
- Poids [select]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Commande\Listing getByNom ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Commande\Listing getByPrenom ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Commande\Listing getByMail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Commande\Listing getByCommentaire ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Commande\Listing getByLivraison ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Commande\Listing getByDate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Commande\Listing getByNombre ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Commande\Listing getByVolume ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Commande\Listing getByPoids ($value, $limit = 0) 
*/

class Commande extends Concrete {

public $o_classId = 1;
public $o_className = "commande";
public $Nom;
public $Prenom;
public $Mail;
public $Commentaire;
public $Livraison;
public $Date;
public $Nombre;
public $Volume;
public $Poids;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Commande
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get Nom - Nom
* @return string
*/
public function getNom () {
	$preValue = $this->preGetValue("Nom"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Nom;
	return $data;
}

/**
* Set Nom - Nom
* @param string $Nom
* @return \Pimcore\Model\DataObject\Commande
*/
public function setNom ($Nom) {
	$this->Nom = $Nom;
	return $this;
}

/**
* Get Prenom - Prenom
* @return string
*/
public function getPrenom () {
	$preValue = $this->preGetValue("Prenom"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Prenom;
	return $data;
}

/**
* Set Prenom - Prenom
* @param string $Prenom
* @return \Pimcore\Model\DataObject\Commande
*/
public function setPrenom ($Prenom) {
	$this->Prenom = $Prenom;
	return $this;
}

/**
* Get Mail - Mail
* @return string
*/
public function getMail () {
	$preValue = $this->preGetValue("Mail"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Mail;
	return $data;
}

/**
* Set Mail - Mail
* @param string $Mail
* @return \Pimcore\Model\DataObject\Commande
*/
public function setMail ($Mail) {
	$this->Mail = $Mail;
	return $this;
}

/**
* Get Commentaire - Commentaire
* @return string
*/
public function getCommentaire () {
	$preValue = $this->preGetValue("Commentaire"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Commentaire;
	return $data;
}

/**
* Set Commentaire - Commentaire
* @param string $Commentaire
* @return \Pimcore\Model\DataObject\Commande
*/
public function setCommentaire ($Commentaire) {
	$this->Commentaire = $Commentaire;
	return $this;
}

/**
* Get Livraison - Livraison
* @return string
*/
public function getLivraison () {
	$preValue = $this->preGetValue("Livraison"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Livraison;
	return $data;
}

/**
* Set Livraison - Livraison
* @param string $Livraison
* @return \Pimcore\Model\DataObject\Commande
*/
public function setLivraison ($Livraison) {
	$this->Livraison = $Livraison;
	return $this;
}

/**
* Get Date - Date
* @return \Carbon\Carbon
*/
public function getDate () {
	$preValue = $this->preGetValue("Date"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Date;
	return $data;
}

/**
* Set Date - Date
* @param \Carbon\Carbon $Date
* @return \Pimcore\Model\DataObject\Commande
*/
public function setDate ($Date) {
	$this->Date = $Date;
	return $this;
}

/**
* Get Nombre - Nombre
* @return float
*/
public function getNombre () {
	$preValue = $this->preGetValue("Nombre"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Nombre;
	return $data;
}

/**
* Set Nombre - Nombre
* @param float $Nombre
* @return \Pimcore\Model\DataObject\Commande
*/
public function setNombre ($Nombre) {
	$this->Nombre = $Nombre;
	return $this;
}

/**
* Get Volume - Volume
* @return string
*/
public function getVolume () {
	$preValue = $this->preGetValue("Volume"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Volume;
	return $data;
}

/**
* Set Volume - Volume
* @param string $Volume
* @return \Pimcore\Model\DataObject\Commande
*/
public function setVolume ($Volume) {
	$this->Volume = $Volume;
	return $this;
}

/**
* Get Poids - Poids
* @return string
*/
public function getPoids () {
	$preValue = $this->preGetValue("Poids"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Poids;
	return $data;
}

/**
* Set Poids - Poids
* @param string $Poids
* @return \Pimcore\Model\DataObject\Commande
*/
public function setPoids ($Poids) {
	$this->Poids = $Poids;
	return $this;
}

protected static $_relationFields = array (
);

public $lazyLoadedFields = array (
);

}

