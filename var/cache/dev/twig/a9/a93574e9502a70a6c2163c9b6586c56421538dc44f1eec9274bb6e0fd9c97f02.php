<?php

/* @WebProfiler/Profiler/header.html.twig */
class __TwigTemplate_0ec3bc96c09455de659cbf9d01e3a5ea6eca854e7df121e9c607918ae0f81cf7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21ced6b71ea7566edb3cafa4367fea60780a3cd7ab38227e5f594bf963faf81e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21ced6b71ea7566edb3cafa4367fea60780a3cd7ab38227e5f594bf963faf81e->enter($__internal_21ced6b71ea7566edb3cafa4367fea60780a3cd7ab38227e5f594bf963faf81e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/header.html.twig"));

        $__internal_756f3937a119b10c052bc81058562b16718cc7341a5631cc2ae3ca976d731662 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_756f3937a119b10c052bc81058562b16718cc7341a5631cc2ae3ca976d731662->enter($__internal_756f3937a119b10c052bc81058562b16718cc7341a5631cc2ae3ca976d731662_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_21ced6b71ea7566edb3cafa4367fea60780a3cd7ab38227e5f594bf963faf81e->leave($__internal_21ced6b71ea7566edb3cafa4367fea60780a3cd7ab38227e5f594bf963faf81e_prof);

        
        $__internal_756f3937a119b10c052bc81058562b16718cc7341a5631cc2ae3ca976d731662->leave($__internal_756f3937a119b10c052bc81058562b16718cc7341a5631cc2ae3ca976d731662_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "@WebProfiler/Profiler/header.html.twig", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\header.html.twig");
    }
}
