<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_1a7010ea81ac9af4eba03c3efcc8303537d9f99fe7da2e2a69d133747c327e66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01f2bf877c95fa2971d7e9e12b767554cef663caa6eeb8c4edf45aa6d649086e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01f2bf877c95fa2971d7e9e12b767554cef663caa6eeb8c4edf45aa6d649086e->enter($__internal_01f2bf877c95fa2971d7e9e12b767554cef663caa6eeb8c4edf45aa6d649086e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_939844303c28e7a0a05cb01eb562f2179e9e6cdd9cc20298f447f37073ec8690 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_939844303c28e7a0a05cb01eb562f2179e9e6cdd9cc20298f447f37073ec8690->enter($__internal_939844303c28e7a0a05cb01eb562f2179e9e6cdd9cc20298f447f37073ec8690_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_01f2bf877c95fa2971d7e9e12b767554cef663caa6eeb8c4edf45aa6d649086e->leave($__internal_01f2bf877c95fa2971d7e9e12b767554cef663caa6eeb8c4edf45aa6d649086e_prof);

        
        $__internal_939844303c28e7a0a05cb01eb562f2179e9e6cdd9cc20298f447f37073ec8690->leave($__internal_939844303c28e7a0a05cb01eb562f2179e9e6cdd9cc20298f447f37073ec8690_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_29f8bae897381885d3868766e6e767423c2770918309963d4b7eb8f5f15b11ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29f8bae897381885d3868766e6e767423c2770918309963d4b7eb8f5f15b11ad->enter($__internal_29f8bae897381885d3868766e6e767423c2770918309963d4b7eb8f5f15b11ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_901163c67e447929aa0344e54ee58c7b629755432e2965b9f198d76fe82bbebf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_901163c67e447929aa0344e54ee58c7b629755432e2965b9f198d76fe82bbebf->enter($__internal_901163c67e447929aa0344e54ee58c7b629755432e2965b9f198d76fe82bbebf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_901163c67e447929aa0344e54ee58c7b629755432e2965b9f198d76fe82bbebf->leave($__internal_901163c67e447929aa0344e54ee58c7b629755432e2965b9f198d76fe82bbebf_prof);

        
        $__internal_29f8bae897381885d3868766e6e767423c2770918309963d4b7eb8f5f15b11ad->leave($__internal_29f8bae897381885d3868766e6e767423c2770918309963d4b7eb8f5f15b11ad_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_789f91e7ee669d0c766126b19ad2e841e75d4bd92968b253bb2ff11be962e3bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_789f91e7ee669d0c766126b19ad2e841e75d4bd92968b253bb2ff11be962e3bd->enter($__internal_789f91e7ee669d0c766126b19ad2e841e75d4bd92968b253bb2ff11be962e3bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_e84781e032fade331dc5dbc982a62ce94f9577f63114bda2da73a795e0537432 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e84781e032fade331dc5dbc982a62ce94f9577f63114bda2da73a795e0537432->enter($__internal_e84781e032fade331dc5dbc982a62ce94f9577f63114bda2da73a795e0537432_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 137, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $__internal_e84781e032fade331dc5dbc982a62ce94f9577f63114bda2da73a795e0537432->leave($__internal_e84781e032fade331dc5dbc982a62ce94f9577f63114bda2da73a795e0537432_prof);

        
        $__internal_789f91e7ee669d0c766126b19ad2e841e75d4bd92968b253bb2ff11be962e3bd->leave($__internal_789f91e7ee669d0c766126b19ad2e841e75d4bd92968b253bb2ff11be962e3bd_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_0c5051898e44245766632feaeccda266a54781b756c40fbfb95405aaf721f93d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c5051898e44245766632feaeccda266a54781b756c40fbfb95405aaf721f93d->enter($__internal_0c5051898e44245766632feaeccda266a54781b756c40fbfb95405aaf721f93d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ebfce7add5fef930e7b564fb08e30b00d263d473c7f3fec823cae7d0e11cd6cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ebfce7add5fef930e7b564fb08e30b00d263d473c7f3fec823cae7d0e11cd6cb->enter($__internal_ebfce7add5fef930e7b564fb08e30b00d263d473c7f3fec823cae7d0e11cd6cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_ebfce7add5fef930e7b564fb08e30b00d263d473c7f3fec823cae7d0e11cd6cb->leave($__internal_ebfce7add5fef930e7b564fb08e30b00d263d473c7f3fec823cae7d0e11cd6cb_prof);

        
        $__internal_0c5051898e44245766632feaeccda266a54781b756c40fbfb95405aaf721f93d->leave($__internal_0c5051898e44245766632feaeccda266a54781b756c40fbfb95405aaf721f93d_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
