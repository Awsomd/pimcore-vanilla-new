<?php

/* PimcoreCoreBundle:Profiler:data_collector.html.twig */
class __TwigTemplate_1b054abad1001895e52a963f47aa0f38df14c872c5e916f9cd1a44c410830445 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebProfilerBundle:Profiler:layout.html.twig", "PimcoreCoreBundle:Profiler:data_collector.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebProfilerBundle:Profiler:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a7d210d5a739abfd7aca9d0a1a0cf7b6a5d03d750117c552a9c59afa306c0ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a7d210d5a739abfd7aca9d0a1a0cf7b6a5d03d750117c552a9c59afa306c0ef->enter($__internal_7a7d210d5a739abfd7aca9d0a1a0cf7b6a5d03d750117c552a9c59afa306c0ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:data_collector.html.twig"));

        $__internal_ce76bab58a5a57b7d98c007c930031ed7c47499da5e16e82fcaafebb248c29ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce76bab58a5a57b7d98c007c930031ed7c47499da5e16e82fcaafebb248c29ab->enter($__internal_ce76bab58a5a57b7d98c007c930031ed7c47499da5e16e82fcaafebb248c29ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:data_collector.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7a7d210d5a739abfd7aca9d0a1a0cf7b6a5d03d750117c552a9c59afa306c0ef->leave($__internal_7a7d210d5a739abfd7aca9d0a1a0cf7b6a5d03d750117c552a9c59afa306c0ef_prof);

        
        $__internal_ce76bab58a5a57b7d98c007c930031ed7c47499da5e16e82fcaafebb248c29ab->leave($__internal_ce76bab58a5a57b7d98c007c930031ed7c47499da5e16e82fcaafebb248c29ab_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_5bdd7f656354f498fadb3179c1b24d07502c8e68f0f9ceea89dcf701f34fc691 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5bdd7f656354f498fadb3179c1b24d07502c8e68f0f9ceea89dcf701f34fc691->enter($__internal_5bdd7f656354f498fadb3179c1b24d07502c8e68f0f9ceea89dcf701f34fc691_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_0977cf3bddae8d053086839bcf0564fddeeacef24c261ed55774e4d3b2da2b92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0977cf3bddae8d053086839bcf0564fddeeacef24c261ed55774e4d3b2da2b92->enter($__internal_0977cf3bddae8d053086839bcf0564fddeeacef24c261ed55774e4d3b2da2b92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        // line 6
        echo "        <div style=\"padding-top: 3px\">
            ";
        // line 7
        echo twig_include($this->env, $context, "PimcoreCoreBundle:Profiler:logo.svg.twig");
        echo "
        </div>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 10
        echo "
    ";
        // line 11
        ob_start();
        // line 12
        echo "        ";
        // line 14
        echo "        <div class=\"sf-toolbar-info-piece\">
            <b>Version</b>
            <span>";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "version", array()), "html", null, true);
        echo " build ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "revision", array()), "html", null, true);
        echo "</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Debug Mode</b>
            <span>";
        // line 21
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 21, $this->getSourceContext()); })()), "debugMode", array())) ? ("enabled") : ("disabled"));
        echo "</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Dev Mode</b>
            <span>";
        // line 26
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 26, $this->getSourceContext()); })()), "devMode", array())) ? ("enabled") : ("disabled"));
        echo "</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Context</b>
            <span>";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 31, $this->getSourceContext()); })()), "context", array()), "html", null, true);
        echo "</span>
        </div>
    ";
        $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 34
        echo "
    ";
        // line 37
        echo "    ";
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => true));
        echo "
";
        
        $__internal_0977cf3bddae8d053086839bcf0564fddeeacef24c261ed55774e4d3b2da2b92->leave($__internal_0977cf3bddae8d053086839bcf0564fddeeacef24c261ed55774e4d3b2da2b92_prof);

        
        $__internal_5bdd7f656354f498fadb3179c1b24d07502c8e68f0f9ceea89dcf701f34fc691->leave($__internal_5bdd7f656354f498fadb3179c1b24d07502c8e68f0f9ceea89dcf701f34fc691_prof);

    }

    // line 40
    public function block_menu($context, array $blocks = array())
    {
        $__internal_10c32eb799e21bdcd7567384656f1bdaa624d5ac1feeeca42d1974784b20aee0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_10c32eb799e21bdcd7567384656f1bdaa624d5ac1feeeca42d1974784b20aee0->enter($__internal_10c32eb799e21bdcd7567384656f1bdaa624d5ac1feeeca42d1974784b20aee0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_716b019137417e26f4a5fac61551260afd6aa4713558337c63405b6f2bd9db2c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_716b019137417e26f4a5fac61551260afd6aa4713558337c63405b6f2bd9db2c->enter($__internal_716b019137417e26f4a5fac61551260afd6aa4713558337c63405b6f2bd9db2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 41
        echo "    <span class=\"label\">
        <span class=\"icon\">
            ";
        // line 43
        echo twig_include($this->env, $context, "PimcoreCoreBundle:Profiler:logo.svg.twig");
        echo "
        </span>
        <strong>Pimcore</strong>
    </span>
";
        
        $__internal_716b019137417e26f4a5fac61551260afd6aa4713558337c63405b6f2bd9db2c->leave($__internal_716b019137417e26f4a5fac61551260afd6aa4713558337c63405b6f2bd9db2c_prof);

        
        $__internal_10c32eb799e21bdcd7567384656f1bdaa624d5ac1feeeca42d1974784b20aee0->leave($__internal_10c32eb799e21bdcd7567384656f1bdaa624d5ac1feeeca42d1974784b20aee0_prof);

    }

    // line 49
    public function block_panel($context, array $blocks = array())
    {
        $__internal_9f42df5c262a7fe0cee6e52fbee9584d8c3e48795acccff9937e1ce6c02a607f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9f42df5c262a7fe0cee6e52fbee9584d8c3e48795acccff9937e1ce6c02a607f->enter($__internal_9f42df5c262a7fe0cee6e52fbee9584d8c3e48795acccff9937e1ce6c02a607f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_04a808e991dcd4c332ed1e578453a06e8336e0efc4145a38389f6877bcfbf6b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04a808e991dcd4c332ed1e578453a06e8336e0efc4145a38389f6877bcfbf6b6->enter($__internal_04a808e991dcd4c332ed1e578453a06e8336e0efc4145a38389f6877bcfbf6b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 50
        echo "    <h2>Pimcore</h2>

    <div class=\"metrics\">
        <div class=\"metric\">
            <span class=\"value\">";
        // line 54
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 54, $this->getSourceContext()); })()), "version", array()), "html", null, true);
        echo "</span>
            <span class=\"label\">Version</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 59
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 59, $this->getSourceContext()); })()), "revision", array()), "html", null, true);
        echo "</span>
            <span class=\"label\">Build</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 64
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 64, $this->getSourceContext()); })()), "debugMode", array())) ? ("enabled") : ("disabled"));
        echo "</span>
            <span class=\"label\">Debug Mode</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 69
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 69, $this->getSourceContext()); })()), "devMode", array())) ? ("enabled") : ("disabled"));
        echo "</span>
            <span class=\"label\">Dev Mode</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 74
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 74, $this->getSourceContext()); })()), "context", array()), "html", null, true);
        echo "</span>
            <span class=\"label\">Request Context</span>
        </div>
    </div>
";
        
        $__internal_04a808e991dcd4c332ed1e578453a06e8336e0efc4145a38389f6877bcfbf6b6->leave($__internal_04a808e991dcd4c332ed1e578453a06e8336e0efc4145a38389f6877bcfbf6b6_prof);

        
        $__internal_9f42df5c262a7fe0cee6e52fbee9584d8c3e48795acccff9937e1ce6c02a607f->leave($__internal_9f42df5c262a7fe0cee6e52fbee9584d8c3e48795acccff9937e1ce6c02a607f_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Profiler:data_collector.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 74,  191 => 69,  183 => 64,  175 => 59,  167 => 54,  161 => 50,  152 => 49,  137 => 43,  133 => 41,  124 => 40,  111 => 37,  108 => 34,  102 => 31,  94 => 26,  86 => 21,  76 => 16,  72 => 14,  70 => 12,  68 => 11,  65 => 10,  59 => 7,  56 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'WebProfilerBundle:Profiler:layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {# this is the content displayed as a panel in the toolbar #}
        <div style=\"padding-top: 3px\">
            {{ include(\"PimcoreCoreBundle:Profiler:logo.svg.twig\") }}
        </div>
    {% endset %}

    {% set text %}
        {# this is the content displayed when hovering the mouse over
           the toolbar panel #}
        <div class=\"sf-toolbar-info-piece\">
            <b>Version</b>
            <span>{{ collector.version }} build {{ collector.revision }}</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Debug Mode</b>
            <span>{{ collector.debugMode ? 'enabled' : 'disabled' }}</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Dev Mode</b>
            <span>{{ collector.devMode ? 'enabled' : 'disabled' }}</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Context</b>
            <span>{{ collector.context }}</span>
        </div>
    {% endset %}

    {# the 'link' value set to 'false' means that this panel doesn't
       show a section in the web profiler #}
    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: true }) }}
{% endblock %}

{% block menu %}
    <span class=\"label\">
        <span class=\"icon\">
            {{ include(\"PimcoreCoreBundle:Profiler:logo.svg.twig\") }}
        </span>
        <strong>Pimcore</strong>
    </span>
{% endblock %}

{% block panel %}
    <h2>Pimcore</h2>

    <div class=\"metrics\">
        <div class=\"metric\">
            <span class=\"value\">{{ collector.version }}</span>
            <span class=\"label\">Version</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ collector.revision }}</span>
            <span class=\"label\">Build</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ collector.debugMode ? 'enabled' : 'disabled' }}</span>
            <span class=\"label\">Debug Mode</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ collector.devMode ? 'enabled' : 'disabled' }}</span>
            <span class=\"label\">Dev Mode</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ collector.context }}</span>
            <span class=\"label\">Request Context</span>
        </div>
    </div>
{% endblock %}
", "PimcoreCoreBundle:Profiler:data_collector.html.twig", "C:\\wamp64\\www\\pimcore-vanilla\\pimcore\\lib\\Pimcore\\Bundle\\CoreBundle/Resources/views/Profiler/data_collector.html.twig");
    }
}
