<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_bb9d1e127889635f53a7518e272301a499b758a533139328704bedcb47d424ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c7aacc0e3e21488d17ae6df522a117bf21cda62652a632d260a09750b709a1ea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c7aacc0e3e21488d17ae6df522a117bf21cda62652a632d260a09750b709a1ea->enter($__internal_c7aacc0e3e21488d17ae6df522a117bf21cda62652a632d260a09750b709a1ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_3e4cddf8ed22115a91d91bea5138a56d7bc9efa137b9ba49e8d2a69d11bbc484 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e4cddf8ed22115a91d91bea5138a56d7bc9efa137b9ba49e8d2a69d11bbc484->enter($__internal_3e4cddf8ed22115a91d91bea5138a56d7bc9efa137b9ba49e8d2a69d11bbc484_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_c7aacc0e3e21488d17ae6df522a117bf21cda62652a632d260a09750b709a1ea->leave($__internal_c7aacc0e3e21488d17ae6df522a117bf21cda62652a632d260a09750b709a1ea_prof);

        
        $__internal_3e4cddf8ed22115a91d91bea5138a56d7bc9efa137b9ba49e8d2a69d11bbc484->leave($__internal_3e4cddf8ed22115a91d91bea5138a56d7bc9efa137b9ba49e8d2a69d11bbc484_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\chevron-right.svg");
    }
}
