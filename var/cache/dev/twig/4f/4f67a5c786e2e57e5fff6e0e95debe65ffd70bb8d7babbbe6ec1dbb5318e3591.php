<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_cc07a221e8216aa2d5c47f3daee79cbf94f8a838ae57e40a5f92a8af66debd07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d93026e370559929c4f637dfc14e70d65da5ed8a421b600162b4a34da1c2d787 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d93026e370559929c4f637dfc14e70d65da5ed8a421b600162b4a34da1c2d787->enter($__internal_d93026e370559929c4f637dfc14e70d65da5ed8a421b600162b4a34da1c2d787_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_f18d4115e3e7ddc3523bbed6fcff51a5ffbe5ebbcf356a56c165cd9a483714ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f18d4115e3e7ddc3523bbed6fcff51a5ffbe5ebbcf356a56c165cd9a483714ff->enter($__internal_f18d4115e3e7ddc3523bbed6fcff51a5ffbe5ebbcf356a56c165cd9a483714ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d93026e370559929c4f637dfc14e70d65da5ed8a421b600162b4a34da1c2d787->leave($__internal_d93026e370559929c4f637dfc14e70d65da5ed8a421b600162b4a34da1c2d787_prof);

        
        $__internal_f18d4115e3e7ddc3523bbed6fcff51a5ffbe5ebbcf356a56c165cd9a483714ff->leave($__internal_f18d4115e3e7ddc3523bbed6fcff51a5ffbe5ebbcf356a56c165cd9a483714ff_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a18794633b5eea08370744261c57af9c74256cea1139053aab9b6b143b55815a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a18794633b5eea08370744261c57af9c74256cea1139053aab9b6b143b55815a->enter($__internal_a18794633b5eea08370744261c57af9c74256cea1139053aab9b6b143b55815a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_dd1197504bf6aeb9d93bc8b0231caf2efe85ae1a5bcb7018cd7494593f8bf768 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd1197504bf6aeb9d93bc8b0231caf2efe85ae1a5bcb7018cd7494593f8bf768->enter($__internal_dd1197504bf6aeb9d93bc8b0231caf2efe85ae1a5bcb7018cd7494593f8bf768_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_dd1197504bf6aeb9d93bc8b0231caf2efe85ae1a5bcb7018cd7494593f8bf768->leave($__internal_dd1197504bf6aeb9d93bc8b0231caf2efe85ae1a5bcb7018cd7494593f8bf768_prof);

        
        $__internal_a18794633b5eea08370744261c57af9c74256cea1139053aab9b6b143b55815a->leave($__internal_a18794633b5eea08370744261c57af9c74256cea1139053aab9b6b143b55815a_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_a2eb75ef2b079c5ccc39c9443dabbe0da4b2698b8a7d696d9f1417efc79a7d65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2eb75ef2b079c5ccc39c9443dabbe0da4b2698b8a7d696d9f1417efc79a7d65->enter($__internal_a2eb75ef2b079c5ccc39c9443dabbe0da4b2698b8a7d696d9f1417efc79a7d65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_c1887154c11d54ee4cd7afb1be2884d4fe5fa3cd94d371a9af40550e8a414294 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1887154c11d54ee4cd7afb1be2884d4fe5fa3cd94d371a9af40550e8a414294->enter($__internal_c1887154c11d54ee4cd7afb1be2884d4fe5fa3cd94d371a9af40550e8a414294_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_c1887154c11d54ee4cd7afb1be2884d4fe5fa3cd94d371a9af40550e8a414294->leave($__internal_c1887154c11d54ee4cd7afb1be2884d4fe5fa3cd94d371a9af40550e8a414294_prof);

        
        $__internal_a2eb75ef2b079c5ccc39c9443dabbe0da4b2698b8a7d696d9f1417efc79a7d65->leave($__internal_a2eb75ef2b079c5ccc39c9443dabbe0da4b2698b8a7d696d9f1417efc79a7d65_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_e426b1ac27a9615c84cdadf28e44d410c9b42b58aca08e4acc9f349cc8911a28 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e426b1ac27a9615c84cdadf28e44d410c9b42b58aca08e4acc9f349cc8911a28->enter($__internal_e426b1ac27a9615c84cdadf28e44d410c9b42b58aca08e4acc9f349cc8911a28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_9c13fc588c32ce74fa5355b4cccc1cf9f335f96f44cc1f2c62251a5195c14c58 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c13fc588c32ce74fa5355b4cccc1cf9f335f96f44cc1f2c62251a5195c14c58->enter($__internal_9c13fc588c32ce74fa5355b4cccc1cf9f335f96f44cc1f2c62251a5195c14c58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 33, $this->getSourceContext()); })()))));
            echo "
        </div>
    ";
        }
        
        $__internal_9c13fc588c32ce74fa5355b4cccc1cf9f335f96f44cc1f2c62251a5195c14c58->leave($__internal_9c13fc588c32ce74fa5355b4cccc1cf9f335f96f44cc1f2c62251a5195c14c58_prof);

        
        $__internal_e426b1ac27a9615c84cdadf28e44d410c9b42b58aca08e4acc9f349cc8911a28->leave($__internal_e426b1ac27a9615c84cdadf28e44d410c9b42b58aca08e4acc9f349cc8911a28_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
