<?php

/* @WebProfiler/Icon/validator.svg */
class __TwigTemplate_e4679fefbdd1560f66666c344dc15e535284ab2f869560d93b090e71a354d104 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32e0545297104338f235935853f49deca1f246244495c9781e9cc92192e5d2f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_32e0545297104338f235935853f49deca1f246244495c9781e9cc92192e5d2f1->enter($__internal_32e0545297104338f235935853f49deca1f246244495c9781e9cc92192e5d2f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/validator.svg"));

        $__internal_b423a106eaf5945c9c890ecc58595784263aa4370d1d3bece2ebd030ccb429c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b423a106eaf5945c9c890ecc58595784263aa4370d1d3bece2ebd030ccb429c7->enter($__internal_b423a106eaf5945c9c890ecc58595784263aa4370d1d3bece2ebd030ccb429c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/validator.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\"><path fill=\"#aaaaaa\" d=\"M19.54,22.5H4.29a2.88,2.88,0,0,1-2.87-2.87V4.37A2.88,2.88,0,0,1,4.29,1.5H18.54a1,1,0,0,1,0,2H4.29a.88.88,0,0,0-.87.87V19.63a.88.88,0,0,0,.87.87H19.54a.88.88,0,0,0,.87-.87V11.29a1,1,0,1,1,2,0v8.33A2.88,2.88,0,0,1,19.54,22.5ZM13,17.29,22.88,6a1.5,1.5,0,1,0-2.26-2L12,14,8,9.11A1.5,1.5,0,0,0,5.65,11l5.1,6.25a1.5,1.5,0,0,0,1.14.55h0A1.5,1.5,0,0,0,13,17.29Z\"/></svg>
";
        
        $__internal_32e0545297104338f235935853f49deca1f246244495c9781e9cc92192e5d2f1->leave($__internal_32e0545297104338f235935853f49deca1f246244495c9781e9cc92192e5d2f1_prof);

        
        $__internal_b423a106eaf5945c9c890ecc58595784263aa4370d1d3bece2ebd030ccb429c7->leave($__internal_b423a106eaf5945c9c890ecc58595784263aa4370d1d3bece2ebd030ccb429c7_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/validator.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\"><path fill=\"#aaaaaa\" d=\"M19.54,22.5H4.29a2.88,2.88,0,0,1-2.87-2.87V4.37A2.88,2.88,0,0,1,4.29,1.5H18.54a1,1,0,0,1,0,2H4.29a.88.88,0,0,0-.87.87V19.63a.88.88,0,0,0,.87.87H19.54a.88.88,0,0,0,.87-.87V11.29a1,1,0,1,1,2,0v8.33A2.88,2.88,0,0,1,19.54,22.5ZM13,17.29,22.88,6a1.5,1.5,0,1,0-2.26-2L12,14,8,9.11A1.5,1.5,0,0,0,5.65,11l5.1,6.25a1.5,1.5,0,0,0,1.14.55h0A1.5,1.5,0,0,0,13,17.29Z\"/></svg>
", "@WebProfiler/Icon/validator.svg", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Icon\\validator.svg");
    }
}
