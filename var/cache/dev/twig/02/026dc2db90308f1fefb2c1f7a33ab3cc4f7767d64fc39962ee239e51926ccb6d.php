<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_70b5dd80e43275ba883586bdcf51adba60031659085a0dbd9c108df1b65f9118 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_63fb9d138ed1005724ff3cf056aadebbd03c4ead13f4aa3526e82d3058abec39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_63fb9d138ed1005724ff3cf056aadebbd03c4ead13f4aa3526e82d3058abec39->enter($__internal_63fb9d138ed1005724ff3cf056aadebbd03c4ead13f4aa3526e82d3058abec39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_0d84e1f178ed08580daf9de5fdcedd53dcc7a270f676ab44f12bd68b7627ff8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d84e1f178ed08580daf9de5fdcedd53dcc7a270f676ab44f12bd68b7627ff8c->enter($__internal_0d84e1f178ed08580daf9de5fdcedd53dcc7a270f676ab44f12bd68b7627ff8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_63fb9d138ed1005724ff3cf056aadebbd03c4ead13f4aa3526e82d3058abec39->leave($__internal_63fb9d138ed1005724ff3cf056aadebbd03c4ead13f4aa3526e82d3058abec39_prof);

        
        $__internal_0d84e1f178ed08580daf9de5fdcedd53dcc7a270f676ab44f12bd68b7627ff8c->leave($__internal_0d84e1f178ed08580daf9de5fdcedd53dcc7a270f676ab44f12bd68b7627ff8c_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_1c554292173f20672371b0a303e2028de8526c9507c32126dbba28bc4d081025 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c554292173f20672371b0a303e2028de8526c9507c32126dbba28bc4d081025->enter($__internal_1c554292173f20672371b0a303e2028de8526c9507c32126dbba28bc4d081025_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_9ad0868b35c9f918243fda9610c062a1c40be7c007997d003f0023a0b9788907 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ad0868b35c9f918243fda9610c062a1c40be7c007997d003f0023a0b9788907->enter($__internal_9ad0868b35c9f918243fda9610c062a1c40be7c007997d003f0023a0b9788907_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_9ad0868b35c9f918243fda9610c062a1c40be7c007997d003f0023a0b9788907->leave($__internal_9ad0868b35c9f918243fda9610c062a1c40be7c007997d003f0023a0b9788907_prof);

        
        $__internal_1c554292173f20672371b0a303e2028de8526c9507c32126dbba28bc4d081025->leave($__internal_1c554292173f20672371b0a303e2028de8526c9507c32126dbba28bc4d081025_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_4af095aa679803ecc805365986b2077a5a52884ad4743540fd08d5b00e8d7db3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4af095aa679803ecc805365986b2077a5a52884ad4743540fd08d5b00e8d7db3->enter($__internal_4af095aa679803ecc805365986b2077a5a52884ad4743540fd08d5b00e8d7db3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_49f02b06e6e2c7c2e836fe2eed1391012e13c2b8c43fdbe8ee056c612180d7a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49f02b06e6e2c7c2e836fe2eed1391012e13c2b8c43fdbe8ee056c612180d7a3->enter($__internal_49f02b06e6e2c7c2e836fe2eed1391012e13c2b8c43fdbe8ee056c612180d7a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_49f02b06e6e2c7c2e836fe2eed1391012e13c2b8c43fdbe8ee056c612180d7a3->leave($__internal_49f02b06e6e2c7c2e836fe2eed1391012e13c2b8c43fdbe8ee056c612180d7a3_prof);

        
        $__internal_4af095aa679803ecc805365986b2077a5a52884ad4743540fd08d5b00e8d7db3->leave($__internal_4af095aa679803ecc805365986b2077a5a52884ad4743540fd08d5b00e8d7db3_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_da7f737bdc145ce03cc63d3570e0dd4458259ccf7c93640b5f977f51e68d0b45 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da7f737bdc145ce03cc63d3570e0dd4458259ccf7c93640b5f977f51e68d0b45->enter($__internal_da7f737bdc145ce03cc63d3570e0dd4458259ccf7c93640b5f977f51e68d0b45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_87a8e725fa37d9dab2a1fa94ab44233c47f8328946cecc971c4bcfc603cbeda7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_87a8e725fa37d9dab2a1fa94ab44233c47f8328946cecc971c4bcfc603cbeda7->enter($__internal_87a8e725fa37d9dab2a1fa94ab44233c47f8328946cecc971c4bcfc603cbeda7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_87a8e725fa37d9dab2a1fa94ab44233c47f8328946cecc971c4bcfc603cbeda7->leave($__internal_87a8e725fa37d9dab2a1fa94ab44233c47f8328946cecc971c4bcfc603cbeda7_prof);

        
        $__internal_da7f737bdc145ce03cc63d3570e0dd4458259ccf7c93640b5f977f51e68d0b45->leave($__internal_da7f737bdc145ce03cc63d3570e0dd4458259ccf7c93640b5f977f51e68d0b45_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
