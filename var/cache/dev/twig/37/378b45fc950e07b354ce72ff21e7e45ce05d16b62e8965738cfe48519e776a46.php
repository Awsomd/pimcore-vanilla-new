<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_f7b66d80321d0732ab4b71d4e11ab84b2f0a85c12054b21ebe7a0cba3b1dba9c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c5965046a890010cec5295ab6fafeef7c69e7bf63ab7c539f31128859151005 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c5965046a890010cec5295ab6fafeef7c69e7bf63ab7c539f31128859151005->enter($__internal_7c5965046a890010cec5295ab6fafeef7c69e7bf63ab7c539f31128859151005_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        $__internal_f077731ac9b47409d3fe6634ace2d1a2d3a1c8a41624bc9f5443d3976de54d45 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f077731ac9b47409d3fe6634ace2d1a2d3a1c8a41624bc9f5443d3976de54d45->enter($__internal_f077731ac9b47409d3fe6634ace2d1a2d3a1c8a41624bc9f5443d3976de54d45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_7c5965046a890010cec5295ab6fafeef7c69e7bf63ab7c539f31128859151005->leave($__internal_7c5965046a890010cec5295ab6fafeef7c69e7bf63ab7c539f31128859151005_prof);

        
        $__internal_f077731ac9b47409d3fe6634ace2d1a2d3a1c8a41624bc9f5443d3976de54d45->leave($__internal_f077731ac9b47409d3fe6634ace2d1a2d3a1c8a41624bc9f5443d3976de54d45_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
", "@Security/Collector/icon.svg", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\SecurityBundle\\Resources\\views\\Collector\\icon.svg");
    }
}
