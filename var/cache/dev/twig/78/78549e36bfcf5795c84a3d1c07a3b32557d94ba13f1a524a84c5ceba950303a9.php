<?php

/* @WebProfiler/Icon/form.svg */
class __TwigTemplate_44d8a0a3802243158428d9c8688450130acc7c6540ca80b6707c81b6dbfdae2c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1bba70913b2d76520e7a2107235389e8bb79620fe62bfda9360d28a65f44bd05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bba70913b2d76520e7a2107235389e8bb79620fe62bfda9360d28a65f44bd05->enter($__internal_1bba70913b2d76520e7a2107235389e8bb79620fe62bfda9360d28a65f44bd05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/form.svg"));

        $__internal_74efac5d20e7bfd0c955e45f696910d675e93193a1681915d95f47c50f5d00d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_74efac5d20e7bfd0c955e45f696910d675e93193a1681915d95f47c50f5d00d0->enter($__internal_74efac5d20e7bfd0c955e45f696910d675e93193a1681915d95f47c50f5d00d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/form.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M20.5,4H18V2.5C18,1.7,17.3,1,16.5,1h-9C6.7,1,6,1.7,6,2.5V4H3.5C2.7,4,2,4.7,2,5.5v16C2,22.3,2.7,23,3.5,
    23h17c0.8,0,1.5-0.7,1.5-1.5v-16C22,4.7,21.3,4,20.5,4z M9,4h6v1H9V4z M19,20H5V7h1.1c0.2,0.6,0.8,1,1.4,1h9c0.7,0,1.2-0.4,1.4-1H19
    V20z M17,11c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1s0.4-1,1-1h8C16.6,10,17,10.4,17,11z M17,14c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1
    s0.4-1,1-1h8C16.6,13,17,13.4,17,14z M13,17c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1s0.4-1,1-1h4C12.6,16,13,16.4,13,17z\"/>
</svg>
";
        
        $__internal_1bba70913b2d76520e7a2107235389e8bb79620fe62bfda9360d28a65f44bd05->leave($__internal_1bba70913b2d76520e7a2107235389e8bb79620fe62bfda9360d28a65f44bd05_prof);

        
        $__internal_74efac5d20e7bfd0c955e45f696910d675e93193a1681915d95f47c50f5d00d0->leave($__internal_74efac5d20e7bfd0c955e45f696910d675e93193a1681915d95f47c50f5d00d0_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/form.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M20.5,4H18V2.5C18,1.7,17.3,1,16.5,1h-9C6.7,1,6,1.7,6,2.5V4H3.5C2.7,4,2,4.7,2,5.5v16C2,22.3,2.7,23,3.5,
    23h17c0.8,0,1.5-0.7,1.5-1.5v-16C22,4.7,21.3,4,20.5,4z M9,4h6v1H9V4z M19,20H5V7h1.1c0.2,0.6,0.8,1,1.4,1h9c0.7,0,1.2-0.4,1.4-1H19
    V20z M17,11c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1s0.4-1,1-1h8C16.6,10,17,10.4,17,11z M17,14c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1
    s0.4-1,1-1h8C16.6,13,17,13.4,17,14z M13,17c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1s0.4-1,1-1h4C12.6,16,13,16.4,13,17z\"/>
</svg>
", "@WebProfiler/Icon/form.svg", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Icon\\form.svg");
    }
}
