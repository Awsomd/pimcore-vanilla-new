<?php

/* @Twig/images/icon-minus-square-o.svg */
class __TwigTemplate_8d47ee9e38c03854c3ac0563c78c0f68e41cbf33d17b1cf73ae3111c56a5fa86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a103e206c7a22b6aa2212e6115d3158db93ac0366dc895d9352bba884052f2b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a103e206c7a22b6aa2212e6115d3158db93ac0366dc895d9352bba884052f2b->enter($__internal_7a103e206c7a22b6aa2212e6115d3158db93ac0366dc895d9352bba884052f2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square-o.svg"));

        $__internal_084276f215c3ac5ff2142f32c9817f6c93225b27ab75408b83a3d396e93e5776 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_084276f215c3ac5ff2142f32c9817f6c93225b27ab75408b83a3d396e93e5776->enter($__internal_084276f215c3ac5ff2142f32c9817f6c93225b27ab75408b83a3d396e93e5776_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square-o.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h832q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
";
        
        $__internal_7a103e206c7a22b6aa2212e6115d3158db93ac0366dc895d9352bba884052f2b->leave($__internal_7a103e206c7a22b6aa2212e6115d3158db93ac0366dc895d9352bba884052f2b_prof);

        
        $__internal_084276f215c3ac5ff2142f32c9817f6c93225b27ab75408b83a3d396e93e5776->leave($__internal_084276f215c3ac5ff2142f32c9817f6c93225b27ab75408b83a3d396e93e5776_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square-o.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h832q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
", "@Twig/images/icon-minus-square-o.svg", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\icon-minus-square-o.svg");
    }
}
