<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_89cffbfa64751451ea47205d1bf909c7ab8eba09069c3048a68935d5d2a2a403 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d12cf2078520adbabbfce79ffa638e2c5ac50117352e37f6ca5310adc129b257 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d12cf2078520adbabbfce79ffa638e2c5ac50117352e37f6ca5310adc129b257->enter($__internal_d12cf2078520adbabbfce79ffa638e2c5ac50117352e37f6ca5310adc129b257_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_0393ff94e82e3bbe04c36e369de2a472915e4b2d0eb657ea2ce810b673cdea32 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0393ff94e82e3bbe04c36e369de2a472915e4b2d0eb657ea2ce810b673cdea32->enter($__internal_0393ff94e82e3bbe04c36e369de2a472915e4b2d0eb657ea2ce810b673cdea32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d12cf2078520adbabbfce79ffa638e2c5ac50117352e37f6ca5310adc129b257->leave($__internal_d12cf2078520adbabbfce79ffa638e2c5ac50117352e37f6ca5310adc129b257_prof);

        
        $__internal_0393ff94e82e3bbe04c36e369de2a472915e4b2d0eb657ea2ce810b673cdea32->leave($__internal_0393ff94e82e3bbe04c36e369de2a472915e4b2d0eb657ea2ce810b673cdea32_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_fd2fe120c2a741d2606a4e1c0140b5bd4a3b927f7042f9d9b8de28df86b19e38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd2fe120c2a741d2606a4e1c0140b5bd4a3b927f7042f9d9b8de28df86b19e38->enter($__internal_fd2fe120c2a741d2606a4e1c0140b5bd4a3b927f7042f9d9b8de28df86b19e38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_5ea11873746188454749c2b3b9b9e623791db6b33476f6d61998a21d09f8de24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ea11873746188454749c2b3b9b9e623791db6b33476f6d61998a21d09f8de24->enter($__internal_5ea11873746188454749c2b3b9b9e623791db6b33476f6d61998a21d09f8de24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_5ea11873746188454749c2b3b9b9e623791db6b33476f6d61998a21d09f8de24->leave($__internal_5ea11873746188454749c2b3b9b9e623791db6b33476f6d61998a21d09f8de24_prof);

        
        $__internal_fd2fe120c2a741d2606a4e1c0140b5bd4a3b927f7042f9d9b8de28df86b19e38->leave($__internal_fd2fe120c2a741d2606a4e1c0140b5bd4a3b927f7042f9d9b8de28df86b19e38_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\ajax.html.twig");
    }
}
