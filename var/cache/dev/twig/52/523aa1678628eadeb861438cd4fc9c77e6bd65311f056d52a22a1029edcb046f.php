<?php

/* PimcoreCoreBundle:Profiler:logo.svg.twig */
class __TwigTemplate_7d27250dafd1fe1bcfe7aeb90f072cea8566db909aec71de1385bb20b2124fef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ff6a26d71e76f01ca748a14fb9f8432552ef755c6360a5b077a3cee04c34f5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ff6a26d71e76f01ca748a14fb9f8432552ef755c6360a5b077a3cee04c34f5e->enter($__internal_0ff6a26d71e76f01ca748a14fb9f8432552ef755c6360a5b077a3cee04c34f5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:logo.svg.twig"));

        $__internal_c28d0e5fc8923d2f46d7f3e10d51e4b0e74a989610247021d3ae9db05d9054fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c28d0e5fc8923d2f46d7f3e10d51e4b0e74a989610247021d3ae9db05d9054fd->enter($__internal_c28d0e5fc8923d2f46d7f3e10d51e4b0e74a989610247021d3ae9db05d9054fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:logo.svg.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 45 26.25\">
    <path d=\"M38.13,16.63a9.37,9.37,0,0,0-7.82,4.21L23.44,31.18a9.38,9.38,0,1,1,0-10.42l1.23,1.86,2.25-3.4-.33-.5a13.12,13.12,0,1,0,0,14.53l2.55-3.85,1.18,1.78a9.38,9.38,0,1,0,7.82-14.55Zm0,15a5.62,5.62,0,0,1-4.7-2.54l-2-3.09,2.06-3.11a5.62,5.62,0,1,1,4.69,8.73Z\" transform=\"translate(-2.5 -12.88)\" style=\"fill:#fff\"/>
</svg>
";
        
        $__internal_0ff6a26d71e76f01ca748a14fb9f8432552ef755c6360a5b077a3cee04c34f5e->leave($__internal_0ff6a26d71e76f01ca748a14fb9f8432552ef755c6360a5b077a3cee04c34f5e_prof);

        
        $__internal_c28d0e5fc8923d2f46d7f3e10d51e4b0e74a989610247021d3ae9db05d9054fd->leave($__internal_c28d0e5fc8923d2f46d7f3e10d51e4b0e74a989610247021d3ae9db05d9054fd_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Profiler:logo.svg.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"utf-8\"?>
<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 45 26.25\">
    <path d=\"M38.13,16.63a9.37,9.37,0,0,0-7.82,4.21L23.44,31.18a9.38,9.38,0,1,1,0-10.42l1.23,1.86,2.25-3.4-.33-.5a13.12,13.12,0,1,0,0,14.53l2.55-3.85,1.18,1.78a9.38,9.38,0,1,0,7.82-14.55Zm0,15a5.62,5.62,0,0,1-4.7-2.54l-2-3.09,2.06-3.11a5.62,5.62,0,1,1,4.69,8.73Z\" transform=\"translate(-2.5 -12.88)\" style=\"fill:#fff\"/>
</svg>
", "PimcoreCoreBundle:Profiler:logo.svg.twig", "C:\\wamp64\\www\\pimcore-vanilla\\pimcore\\lib\\Pimcore\\Bundle\\CoreBundle/Resources/views/Profiler/logo.svg.twig");
    }
}
