<?php

/* PimcoreCoreBundle:Profiler:target.svg.twig */
class __TwigTemplate_e99aa403aa3bf48bd0e6923ae0bc11243b0343d3933a9683094c393ee9da226b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_98b2c7546e8c07b017d79775d0248f85caf5f0e782a749a584be7addfc202082 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_98b2c7546e8c07b017d79775d0248f85caf5f0e782a749a584be7addfc202082->enter($__internal_98b2c7546e8c07b017d79775d0248f85caf5f0e782a749a584be7addfc202082_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:target.svg.twig"));

        $__internal_80e8ad31fe74f2a10fbb62979a5389ccf6f8f5a813cbf0db877f56adb37ccab9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80e8ad31fe74f2a10fbb62979a5389ccf6f8f5a813cbf0db877f56adb37ccab9->enter($__internal_80e8ad31fe74f2a10fbb62979a5389ccf6f8f5a813cbf0db877f56adb37ccab9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:target.svg.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<!-- Generator: Adobe Illustrator 19.2.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version=\"1.1\" id=\"Ebene_1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\"
     viewBox=\"0 0 48 48\" style=\"enable-background:new 0 0 48 48;\" xml:space=\"preserve\">
    <style type=\"text/css\">
        .st0 { fill: #eee; }
        .st1 { fill: #555; }
    </style>

    <circle class=\"st0\" cx=\"24\" cy=\"24\" r=\"21\"/>
    <ellipse class=\"st1\" cx=\"24\" cy=\"24\" rx=\"14\" ry=\"14.2\"/>
    <circle class=\"st0\" cx=\"24\" cy=\"24\" r=\"7.1\"/>
</svg>
";
        
        $__internal_98b2c7546e8c07b017d79775d0248f85caf5f0e782a749a584be7addfc202082->leave($__internal_98b2c7546e8c07b017d79775d0248f85caf5f0e782a749a584be7addfc202082_prof);

        
        $__internal_80e8ad31fe74f2a10fbb62979a5389ccf6f8f5a813cbf0db877f56adb37ccab9->leave($__internal_80e8ad31fe74f2a10fbb62979a5389ccf6f8f5a813cbf0db877f56adb37ccab9_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Profiler:target.svg.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"utf-8\"?>
<!-- Generator: Adobe Illustrator 19.2.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version=\"1.1\" id=\"Ebene_1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\"
     viewBox=\"0 0 48 48\" style=\"enable-background:new 0 0 48 48;\" xml:space=\"preserve\">
    <style type=\"text/css\">
        .st0 { fill: #eee; }
        .st1 { fill: #555; }
    </style>

    <circle class=\"st0\" cx=\"24\" cy=\"24\" r=\"21\"/>
    <ellipse class=\"st1\" cx=\"24\" cy=\"24\" rx=\"14\" ry=\"14.2\"/>
    <circle class=\"st0\" cx=\"24\" cy=\"24\" r=\"7.1\"/>
</svg>
", "PimcoreCoreBundle:Profiler:target.svg.twig", "C:\\wamp64\\www\\pimcore-vanilla\\pimcore\\lib\\Pimcore\\Bundle\\CoreBundle/Resources/views/Profiler/target.svg.twig");
    }
}
