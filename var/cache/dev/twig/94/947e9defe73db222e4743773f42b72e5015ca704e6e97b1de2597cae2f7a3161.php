<?php

/* @Twig/images/icon-plus-square.svg */
class __TwigTemplate_af9666f790230cc6bac2bed9645199a19bdd015a9bcdf3449d0d6c0df29c37da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17baa403a99f49b1e76c62eacee5bc5ddf99935d3597cda7ef63dd54e199031b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17baa403a99f49b1e76c62eacee5bc5ddf99935d3597cda7ef63dd54e199031b->enter($__internal_17baa403a99f49b1e76c62eacee5bc5ddf99935d3597cda7ef63dd54e199031b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        $__internal_bd98beb6b8f227bd2b572ea285fa6c9cd06dc08bdfb46d1354b5ea53f5d77e95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd98beb6b8f227bd2b572ea285fa6c9cd06dc08bdfb46d1354b5ea53f5d77e95->enter($__internal_bd98beb6b8f227bd2b572ea285fa6c9cd06dc08bdfb46d1354b5ea53f5d77e95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
";
        
        $__internal_17baa403a99f49b1e76c62eacee5bc5ddf99935d3597cda7ef63dd54e199031b->leave($__internal_17baa403a99f49b1e76c62eacee5bc5ddf99935d3597cda7ef63dd54e199031b_prof);

        
        $__internal_bd98beb6b8f227bd2b572ea285fa6c9cd06dc08bdfb46d1354b5ea53f5d77e95->leave($__internal_bd98beb6b8f227bd2b572ea285fa6c9cd06dc08bdfb46d1354b5ea53f5d77e95_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-plus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
", "@Twig/images/icon-plus-square.svg", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\icon-plus-square.svg");
    }
}
