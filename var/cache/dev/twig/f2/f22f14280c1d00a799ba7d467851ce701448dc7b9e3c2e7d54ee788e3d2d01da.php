<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_6d52840329bae408b26161baa3f4723aff9c0bb92e94a391798a5bbf6a87dd78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fc7183027de11d58d6f6651075591f304b32ce4cda38e9d15425ea4dccd6a0db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc7183027de11d58d6f6651075591f304b32ce4cda38e9d15425ea4dccd6a0db->enter($__internal_fc7183027de11d58d6f6651075591f304b32ce4cda38e9d15425ea4dccd6a0db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_68c0b3e896ff560a972073981992ccefddda30096621d1d6c53676c380109124 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_68c0b3e896ff560a972073981992ccefddda30096621d1d6c53676c380109124->enter($__internal_68c0b3e896ff560a972073981992ccefddda30096621d1d6c53676c380109124_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_fc7183027de11d58d6f6651075591f304b32ce4cda38e9d15425ea4dccd6a0db->leave($__internal_fc7183027de11d58d6f6651075591f304b32ce4cda38e9d15425ea4dccd6a0db_prof);

        
        $__internal_68c0b3e896ff560a972073981992ccefddda30096621d1d6c53676c380109124->leave($__internal_68c0b3e896ff560a972073981992ccefddda30096621d1d6c53676c380109124_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_96ceb9bc0d360ce09ccf565d495ff4a9ad2a6ed5e228256013895704324d4a98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96ceb9bc0d360ce09ccf565d495ff4a9ad2a6ed5e228256013895704324d4a98->enter($__internal_96ceb9bc0d360ce09ccf565d495ff4a9ad2a6ed5e228256013895704324d4a98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_3581ff37728001ba5c797292b9019cbf8ecef88c5c242b5216e9e7ae7d985b22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3581ff37728001ba5c797292b9019cbf8ecef88c5c242b5216e9e7ae7d985b22->enter($__internal_3581ff37728001ba5c797292b9019cbf8ecef88c5c242b5216e9e7ae7d985b22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_3581ff37728001ba5c797292b9019cbf8ecef88c5c242b5216e9e7ae7d985b22->leave($__internal_3581ff37728001ba5c797292b9019cbf8ecef88c5c242b5216e9e7ae7d985b22_prof);

        
        $__internal_96ceb9bc0d360ce09ccf565d495ff4a9ad2a6ed5e228256013895704324d4a98->leave($__internal_96ceb9bc0d360ce09ccf565d495ff4a9ad2a6ed5e228256013895704324d4a98_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_3c33265729a79d9267ec025613bcdda601e4a60ba58fead9b99f75f329f1a91c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3c33265729a79d9267ec025613bcdda601e4a60ba58fead9b99f75f329f1a91c->enter($__internal_3c33265729a79d9267ec025613bcdda601e4a60ba58fead9b99f75f329f1a91c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_7e19cf03224631a5354ce83206e50c0bbf9bb2f2f93cc9cd7b6cd4c86cbb43e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e19cf03224631a5354ce83206e50c0bbf9bb2f2f93cc9cd7b6cd4c86cbb43e5->enter($__internal_7e19cf03224631a5354ce83206e50c0bbf9bb2f2f93cc9cd7b6cd4c86cbb43e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_7e19cf03224631a5354ce83206e50c0bbf9bb2f2f93cc9cd7b6cd4c86cbb43e5->leave($__internal_7e19cf03224631a5354ce83206e50c0bbf9bb2f2f93cc9cd7b6cd4c86cbb43e5_prof);

        
        $__internal_3c33265729a79d9267ec025613bcdda601e4a60ba58fead9b99f75f329f1a91c->leave($__internal_3c33265729a79d9267ec025613bcdda601e4a60ba58fead9b99f75f329f1a91c_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_442693ff119f4f8cd7e4bca81ed2420150c1c13d47c55532ca57ba6711d50659 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_442693ff119f4f8cd7e4bca81ed2420150c1c13d47c55532ca57ba6711d50659->enter($__internal_442693ff119f4f8cd7e4bca81ed2420150c1c13d47c55532ca57ba6711d50659_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_cd5906eeac3fc31031007300dfc6454d9df340020385d4bec0b2e96937cb0588 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd5906eeac3fc31031007300dfc6454d9df340020385d4bec0b2e96937cb0588->enter($__internal_cd5906eeac3fc31031007300dfc6454d9df340020385d4bec0b2e96937cb0588_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_cd5906eeac3fc31031007300dfc6454d9df340020385d4bec0b2e96937cb0588->leave($__internal_cd5906eeac3fc31031007300dfc6454d9df340020385d4bec0b2e96937cb0588_prof);

        
        $__internal_442693ff119f4f8cd7e4bca81ed2420150c1c13d47c55532ca57ba6711d50659->leave($__internal_442693ff119f4f8cd7e4bca81ed2420150c1c13d47c55532ca57ba6711d50659_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "C:\\wamp64\\www\\pimcore-vanilla\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\layout.html.twig");
    }
}
