<?php

namespace AppBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;


class MapController extends FrontendController
{
    public function defaultAction(Request $request)
    {
      // Check if form was submitted
      if(isset($_POST['submit'])) {
        if (isset( $_POST['livraison'],
                      $_POST['number'],
                      $_POST['volume'],
                      $_POST['weight'],
                      $_POST['totalEuro'],
                      $_POST['prenom'],
                      $_POST['nom'],
                      $_POST['mail'],
                      $_POST['mail-check'],
                      $_POST['message'],
                      $_POST['date'],
                      $_POST['time'])) {
          // Get post data
            $livraison = $_POST['livraison'];
            $number = $_POST['number'];
            $volume = $_POST['volume'];
            $weight = $_POST['weight'];
            $euro = $_POST['totalEuro'];
            $prenom = $_POST['prenom'];
            $nom = $_POST['nom'];
            $mail = $_POST['mail'];
            $mailCheck = $_POST['mail-check'];
            $message = $_POST['message'];
            $date = $_POST['date'];
            $time = $_POST['time'];

            if ($mail != $mailCheck) {
              $errors = "Vérifiez votre adresse mail."
            }
          }
          else {
            $error = "Veuillez compléter tous les champs."
          }
          function randomString($length) {
            $str = "";
            $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
            $max = count($characters) - 1;
            for ($i = 0; $i < $length; $i++) {
              $rand = mt_rand(0, $max);
              $str .= $characters[$rand];
            }
            return $str;
          }

          $orderId = randomString(5);
          $orderName = $nom.' '.$prenom.' '.$orderId;

          // Create a new object
          $newObject = new DataObject\Commande;

          $newObject->setKey(\Pimcore\File::getValidFilename($orderName));
          $newObject->setParentId(1);
          $newObject->setNom($nom);
          $newObject->setPrenom($prenom);
          $newObject->setMail($mail);
          $newObject->setCommentaire($message);
          $newObject->setLivraison($livraison);
          $newObject->setNombre($number);
          $newObject->setVolume($volume);
          $newObject->setPoids($weight);
          $newObject->save();
  /*
        // Send mail
        $mail = new \Pimcore\Mail();
        $mail->addTo('example@pimcore.org');
        $mail->setBodyText("This is just plain text");
        if($asset instanceof Asset) {
           $mail->createAttachment($asset->getData(), $asset->getMimetype(), $asset->getFilename());
        }
        $mail->send();
        */

    }
  }
}
