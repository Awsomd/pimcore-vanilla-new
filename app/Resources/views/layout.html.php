<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Calculateur</title>
      <link rel="stylesheet" type="text/css" href="/static/css/style.css" />
  </head>
  <body>

    <?php $this->slots()->output('_content') ?>

  </body>
</html>
