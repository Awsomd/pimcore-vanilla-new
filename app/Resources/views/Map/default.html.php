<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout.html.php');



?>

<div class="uk-width-medium-7-10 uk-container-center">
  <div id="map-header">
    <div class="uk-float-left">
        <h1>
          <?= $this->wysiwyg("header-title", ["height" => 110]); ?>
        </h1>
    </div>
    <div id="map-inputs" class="uk-float-left">
      <p>
        <label><?= $this->textarea("depart", [ "width" => 80]); ?> </label>
          <input id="origin-input" class="controls" type="text" placeholder="Départ">
      </p>
      <p>
        <label><?= $this->textarea("arrivee", [ "width" => 80]); ?> </label>
          <input id="destination-input" class="controls" type="text" placeholder="Arrivée">
      </p>
    </div>
  </div>
  <div class="uk-overlay">

    <div id="map"><!-- Map rendering --></div>

    <form id="overlay" class="uk-overlay-panel uk-overlay-right" action="" method="post">
      <div class="calculator">
        <h2>
          <?= $this->wysiwyg("calc-title", [
                  "height" => 80
              ]); ?>
        </h2>

        <hr class="uk-divider">

        <div class="uk-form">
            <!-- Get the distance from Google API -->
            <input type="hidden" name="distance" value="0">

            <p>
              <label class="uk-float-left"><?= $this->textarea("type", [ "width" => 100]); ?></label>
              <label class="uk-float-left">
                <span></span>
                <input type="radio" name="livraison"  value="1"> <?= $this->textarea("express", [ "width" => 80]); ?>
              </label>
              <label>
                <input type="radio" name="livraison"  value="0"> <?= $this->textarea("standard", [ "width" => 80]); ?>
              </label>
            </p>
            <p>
              <label  class="uk-float-left"><?= $this->textarea("nombre", [ "width" => 80]); ?> </label>
              <div class="uk-button uk-form-select" data-uk-form-select>
                  <span></span>
                  <select name="number">
                      <option data-nbr="1" value="1">1</option>
                      <option data-nbr="1.75" value="2">2</option>
                      <option data-nbr="2.25" value="3">3</option>
                      <option data-nbr="4.75" value="4">4</option>
                      <option data-nbr="5.25" value="5">5</option>
                  </select>
              </div>
            </p>
            <p>
              <label  class="uk-float-left"><?= $this->textarea("volume", [ "width" => 80]); ?> </label>
              <div class="uk-button uk-form-select" data-uk-form-select>
                  <span></span>
                  <select name="volume">
                      <option data-vol="1" value="1">Enveloppe</option>
                      <option data-vol="3" value="2">Caisse A5</option>
                      <option data-vol="4" value="3">Caisse A4</option>
                      <option data-vol="5" value="4">Caisse A3</option>
                  </select>
              </div>
            </p>
            <p>
              <label  class="uk-float-left"><?= $this->textarea("poids", [ "width" => 80]); ?> </label>
              <div class="uk-button uk-form-select" data-uk-form-select>
                  <span></span>
                  <select name="weight">
                      <option data-weight="1" value="1">100g</option>
                      <option data-weight="5" value="2">500g</option>
                      <option data-weight="10" value="3">1kg</option>
                  </select>
              </div>
            </p>
          </div>
          <hr class="uk-divider">
          <div class="uk-grid">
            <div class="uk-width-1-4">
              <button id="calculate" class="uk-button" type="button" name="button" onclick="formValidate()"><?= $this->textarea("calculer", [ "width" => 80]); ?></button>
            </div>
            <div class="uk-width-3-4 uk-alert uk-alert-warning uk-hidden" data-uk-alert>
              <a href="" class="uk-alert-close uk-close"></a>
              <p>Veuillez choisir une course</p>
            </div>
          </div>
        </div>

        <div class="validation uk-hidden">
          <p class="total">
            Votre total :
            <span id="total"><!-- total --></span>
          </p
          <hr class="uk-divider">
          <p>
            <label>Prénom: </label>
            <input name="prenom" type="text">
            <label>Nom: </label>
            <input name="nom" type="text">
          </p>
          <p>
            <label>Votre email: </label>
            <input name="mail" type="text" placeholder="example@mail.com">
            <label>Vérification: </label>
            <input name="mail-check" type="text" placeholder="example@mail.com">
          </p>
          <p>
            <label>Message (facultatif): </label>
            <div class="uk-form-row">
                <textarea name="message" cols="" rows="5"></textarea>
            </div>
          </p>
          <p>
            <label>Date de retrait: </label>
              <input name="date" type="text" data-uk-datepicker="{format:'DD.MM.YYYY'}">
          </p>
          <p>
            <label>Heure: </label>
              <input type="text" name="time" data-uk-timepicker="{format:'12h'}">
          </p>
          <hr class="uk-divider">
          <input type="hidden" name="totalEuro" value="Erreur lors du calcul">
          <a href="formulaire">Formulaire</a>
          <input id="submit" name="submit" type="submit" value="Send" class="uk-button">
        </div>
    </form>
  </div>
</div>

<script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script async defer>
  function formValidate() {

    var priceKm = 0.5;
    var priceExpress = 10;
    var priceVol = 0.5;
    var priceWeight = 0.5;

    var distance = $("input[name=distance]").attr("value");

      // Triggers only if a destination has been set, other inputs can be default values
    if ( distance > 0) {
      var totalKm = priceKm*distance;
      var totalExpress = 0;
      // If express selected adds up 10€
      if ( $('input[name=livraison]:checked').val() == 1) {
        $('input[name=datepicker]').val('Le lendemain');
        totalExpress = priceExpress;
      }
      var totalNumber = $('select[name=number]').find(':selected').attr("data-nbr");
      var totalVol =  $('select[name=volume]').find(':selected').attr("data-vol")
      var totalWeight = $('select[name=weight]').find(':selected').attr("data-weight")
      var total = totalKm*totalNumber*totalVol*totalWeight+totalExpress;

      // Round to 2 decimals and add € sign
      var totalEuro = Number(Math.round(total +'e2')+'e-2') + '€';
      // Display the total
      $('#total').html(totalEuro);
      // Set total to hidden input field to get posted
      $("input[name=totalEuro]").attr("value", totalEuro);
      // Hide calcultor and show the validate order inputs
      $('.calculator').addClass("uk-hidden");
      $('.validation').removeClass("uk-hidden");
      // Change height of the overlay to adapt to the new content
      $('#overlay').css('height', '360px');
    }
    else {
        // If no destination set show the alert
        $('.uk-alert').removeClass("uk-hidden");
    }
  }
</script>
<script  async defer src="/static/js/scripts-dist.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAh7EfU1XJjYDfoqG5gwrCcldiqdGvsjBY&libraries=places&callback=initMap"
     async defer></script>
